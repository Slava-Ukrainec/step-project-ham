
const servicesDescriptionToggle = () => {
    $(".service-active").removeClass("service-active");
    $(event.target).addClass("service-active");

    let serviceOrder = $(event.target).index();
    let ServicesArr = $(".services-description-item");
    $(ServicesArr).hide();
    $(ServicesArr[serviceOrder]).show();
}

const workExamplesFilter = () => {
    $(".work-examples-navigation-item.color-highlight").removeClass("color-highlight");
    $(event.target).addClass("color-highlight");

    let exampleFilter = $(event.target).attr("data-exampleFilter");
    workExamplesCurFilter = exampleFilter;
    if (exampleFilter == "no-filter") $(".example-active").show()
    else {
        $(`.example-active:not(.${exampleFilter})`).hide();
        $(`.example-active.${exampleFilter}`).show();
    }
}

const loadMoreExamples = () => {
    $(".examples.load-simulator").hide();
    let imgArr;
    if (workExamplesCurFilter == "no-filter") {
        imgArr = $(`.work-examples-piccontainer.disabled`);
    }
    else {
        imgArr = $(`.work-examples-piccontainer.disabled.${workExamplesCurFilter}`);
    }
    if (imgArr.length>12) {imgArr.length = 12};
    $(imgArr).addClass("example-active").removeClass("disabled");
    if ($(".work-examples-piccontainer.disabled").length === 0) $(".MoreExamplesBtn").hide();
}

const moreExamplesBtnClick = () => {
        $(".examples.load-simulator").show();
        setTimeout(loadMoreExamples,2000);
}

function roundaboutUsersClick() {
    $(".feedback-other-users-item-piccontainer.current-user-selection").removeClass("current-user-selection");
    $(this).addClass("current-user-selection");
    let selectedUser = $(this).attr("data-user");

    $(".feedback-article").hide();
    $(`article[data-user='${selectedUser}']`).fadeIn(250);
}

const roundaboutArrows = (arrow) => {
    let curIndex = $(".current-user-selection").index();
    $(".current-user-selection").removeClass("current-user-selection");
    if (arrow === "left") {
        $(".feedback-other-users-item-piccontainer").eq(curIndex-1).addClass("current-user-selection")
    }
    else {
        if (curIndex+1 === $(".feedback-other-users-item-piccontainer").length) {
            $(".feedback-other-users-item-piccontainer").eq(0).addClass("current-user-selection")
        }
        else $(".feedback-other-users-item-piccontainer").eq(curIndex+1).addClass("current-user-selection")
    }

    let selectedUser = $(".current-user-selection").attr("data-user");
    $(".feedback-article").hide();
    $(`.feedback-article[data-user='${selectedUser}']`).fadeIn(250);
}

function userFeedbackScrl(){
    let navBtn
    if ($(this).hasClass("feedbacks-other-users-navigation-button-left")) navBtn = "left";
    roundaboutArrows(navBtn);
    curInterval = setInterval(roundaboutArrows,300,navBtn);
};

const loadMoreGalleryPic = () => {
    $(".gallery.load-simulator").hide();
    let imgArr = $(".grid-item.disabled")
    if (imgArr.length>12) {imgArr.length = 12}
    $(imgArr).removeClass("disabled");
    if ($(".grid-item.disabled").length === 0) $(".MoreGalleryPicBtn").hide();
    $('.grid').masonry('layout');
}

const moreGalleryPicBtnClick = () => {
        $(".gallery.load-simulator").show();
        setTimeout(loadMoreGalleryPic,2000);
}

showGalleryHoverbar = () => {
        let hoverbar = `
            <div class="gallery-hoverbar">
                <div class="gallery-btn gallery-search"><i class="fas fa-search"></i></div>
                <div class="gallery-btn gallery-expand"><span class="icon-enlarge"></span></div>
            </div>
        `
               $(event.target).before(hoverbar)
}

$('.grid').masonry({
  itemSelector: '.grid-item',
  columnWidth: 175,
  gutter: 20
});


$(".service-navigation-bar").on("click",servicesDescriptionToggle);

$(".MoreExamplesBtn").on("click",moreExamplesBtnClick);

let workExamplesCurFilter = "no-filter"
$(".work-examples-navigation-bar").on("click",workExamplesFilter);

$(".feedback-other-users-item-piccontainer").on("click",roundaboutUsersClick);
let curInterval
$(`div[class^='feedbacks-other-users-navigation-button']`)
    .on("mousedown", userFeedbackScrl)
    .on("mouseup",() => clearInterval(curInterval));

$(".MoreGalleryPicBtn").on("click",moreGalleryPicBtnClick);
//
//$(".grid-item img").on("mouseenter",showGalleryHoverbar);
//

